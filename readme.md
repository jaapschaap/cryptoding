# Howto use...

Just upload `crypto.php` on a PHP server. And edit the array (add/remove (alt)coins).

The script will try to write a file `crypto.json` because coinmarketcap-API requests are limited by 5 per minute so caching is required if you are impatient (like me).

# Preview...
![](preview.png)