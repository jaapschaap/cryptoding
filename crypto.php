<style>
body {
	font-family:verdana;
	font-size:12px;
}
span {
	display:inline-block;
	padding:3px;
	border-radius:3px;
}
</style>
<?php
$crypto = array(
	'BTC' => array(
		'amount' => '2.005914', //amount you own
		'BTC' => '2.005914', //when bought, worth in BTC
		'EUR' => '1800' //when bought, worth in EURO's
	),	
	'ETH' => array(
		'amount' => '1', //amount you own
		'BTC' => '0.128855', //when bought, worth in BTC
		'EUR' => '250' //when bought, worth in EURO's
	),
	'DGB' => array(
		'amount' => '1673.45345', //amount you own
		'BTC' => '0.03385', //when bought, worth in BTC
		'EUR' => '75' //when bought, worth in EURO's
	) 
);

$jsonfile_location = 'crypto.json';

$date = filemtime($jsonfile_location);
if($date < (time() - (20))) {
	$myfile = fopen($jsonfile_location, "w") or die("Unable to open file!");
	$crypto_json = file_get_contents('https://api.coinmarketcap.com/v1/ticker/?convert=EUR');
	fwrite($myfile, $crypto_json);
	fclose($myfile);
} else {
	$crypto_json = file_get_contents($jsonfile_location);
}

$crypto_json = json_decode($crypto_json);
$crypto_keys = array_keys($crypto);

$total_portfolio_btc_current = 0;
$total_portfolio_btc_past = 0;
$total_portfolio_euro_current = 0;
$total_portfolio_euro_past = 0;

foreach($crypto_json as $currency) {
	if(in_array($currency->symbol, $crypto_keys)) {
		$current_btc_worth = $crypto[$currency->symbol]['amount'] * ($currency->price_btc);
		$current_eur_worth = $crypto[$currency->symbol]['amount'] * ($currency->price_eur);
		
		$total_portfolio_btc_current += $current_btc_worth;
		$total_portfolio_btc_past += $crypto[$currency->symbol]['BTC'];
		$total_portfolio_euro_current += $current_eur_worth;
		$total_portfolio_euro_past += $crypto[$currency->symbol]['EUR'];
		
		if($currency->symbol != 'BTC') {
			$total_portfolio_without_btc_current += $current_btc_worth;
			$total_portfolio_without_btc_past += $crypto[$currency->symbol]['BTC'];
		}
		
		$profit_color_btc = 'red';
		if($current_btc_worth >= $crypto[$currency->symbol]['BTC']) {
			$profit_color_btc = 'green';
		}
		
		$profit_color_eur = 'red';
		if($current_eur_worth >= $crypto[$currency->symbol]['EUR']) {
			$profit_color_eur = 'green';
		}
		
		$profit_btc = (100 / $crypto[$currency->symbol]['BTC']) * $current_btc_worth;
		if($crypto[$currency->symbol]['EUR'] > 0) {
			$profit_eur = round((100 / $crypto[$currency->symbol]['EUR']) * $current_eur_worth - 100, 2) .'%';			
		} else { 
			$profit_eur = 'endless...';
		}
		
		echo '<h3><a href="https://coinmarketcap.com/currencies/' . $currency->id . '" target="_blank">' . $currency->name . '</a> (Amount: ' . $crypto[$currency->symbol]['amount'] . ')</h3>';
		echo 'EUR value: &euro;' . number_format($crypto[$currency->symbol]['EUR'], 2, '.', '') . ' => &euro;' . number_format($current_eur_worth, 2, '.', '') . '<br />';
		echo 'EUR profit: <span style="background-color:' . $profit_color_eur . '">' . $profit_eur . '</span><br />';
		if($currency->symbol != 'BTC') {
			echo 'BTC value: ' . $crypto[$currency->symbol]['BTC'] . ' => ' . $current_btc_worth . '<br />';
			echo 'BTC profit: <span style="background-color:' . $profit_color_btc . '">' . round($profit_btc - 100, 2) . '%</span><br />';			
		}
	}
}

echo '<h2>Total EUR value: &euro;' . number_format($total_portfolio_euro_past, 2, '.', '') . ' => &euro;' . number_format($total_portfolio_euro_current, 2, '.', '') . '</h2>';
echo '<h2>Total BTC value: ' . $total_portfolio_btc_past . ' => ' . $total_portfolio_btc_current . '</h2>';